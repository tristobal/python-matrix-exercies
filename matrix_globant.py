"""
Create a class which allows to represent a matrix of any order or shape (n x n, n x m).
It should also allow the following operations with matrices:

[X] Matrices addition
[X] Scalar multiplication
[X] Matrices multiplication
[X] Matrix transpose
[X] Matrix determinant
[X] Adjugate matrix
[X] Inverted matrix

Add additional methods:

[ ] A method to obtain a matrix row. Do not return repeated rows. E.g., you can specify that the rows 1,3,8 are desired from a 20x10 matrix.
[ ] A method to obtain a matrix column. Do not return repeated columns. E.g., you can specify that the columns 2,4,10 are desired from a 20x10 matrix.
[ ] A method to obtain part of the matrix from a certain column. E.g., from a 10x10 matrix, the 10x2 matrix from column 5 is desired.
[X] A method to show the matrix on screen as a matrix.


[X] A 3x6 matrix can be defined at the class instantiation time like this:
    Matrix([[1,2,3,4,5,6], [10,11,12,13,14,15], [-1,-2,-3,-4,-5,-6]])
"""


class MatrixError(Exception):
    """Class for any exception in operations"""
    pass


class Matrix:
    rows = []

    def __init__(self, rows):
        self.rows = rows
        self.m = len(rows)
        self.n = len(rows[0])

    def __str__(self):
        s = '\n'.join([' '.join([str(item) for item in row]) for row in self.rows])
        return s + '\n'

    def __getitem__(self, idx):
        return self.rows[idx]

    def get_rank(self):
        return self.m, self.n

    def __add__(self, other):
        if self.get_rank() != other.get_rank():
            raise MatrixError("Both Matrix must have the same rank")
        final_rows = []
        for i in range(self.m):
            aux_row = []
            for j in range(self.n):
                aux_row.append(self.rows[i][j] + other.rows[i][j])
            final_rows.append(aux_row)
        return Matrix(final_rows)

    def scalar_product(self, number):
        final_rows = []
        for i in range(self.m):
            aux_row = []
            for j in range(self.n):
                aux_row.append(self.rows[i][j] * number)
            final_rows.append(aux_row)
        return Matrix(final_rows)

    def get_transpose(self):
        rows = [list(item) for item in zip(*self.rows)]
        return Matrix(rows)

    def __mul__(self, other):
        m, _ = other.get_rank()
        if self.n != m:
            raise MatrixError("The columns of the first matrix must be equals to the rows of the second one")
        row, _ = self.get_rank()
        _, col = other.get_rank()
        final = [[0] * col for x in range(row)]

        for x in range(self.m):
            for y in range(other.get_transpose().m):
                final[x][y] = sum([item[0] * item[1] for item in zip(self.rows[x], other.get_transpose()[y])])
        return Matrix(final)

    def is_square(self):
        m, n = self.get_rank()
        return m == n

    def determinant(self):
        if not self.is_square():
            raise MatrixError("Determinant is only defined for square matrix")

        if len(self.rows) == 1:
            return self.rows[0][0]

        if len(self.rows) == 2:
            val = self.rows[0][0] * self.rows[1][1] - self.rows[1][0] * self.rows[0][1]
            return val

        det = 0
        for i in range(len(A.rows[0])):
            try:
                det += self.rows[0][i] * self.cofactor(0, i)
            except IndexError:
                pass
        return det

    def cofactor(self, row, column):
        """
        https://matematicasies.com/Menor-Complementario,3466
        :param row:
        :param column:
        :return:
        """
        if not self.is_square():
            raise MatrixError("Cofactor is only defined for square matrix")
        return ((-1) ** (row + column)) * self.minor(row, column)

    def minor(self, i, j):
        if not self.is_square():
            raise MatrixError("Minor is only defined for square matrix")
        if self.m == 1 and self.n == 1:
            raise MatrixError("Minor is not defined for 1x1 matrix")
        rows = self.rows
        m = Matrix(rows)
        m = m.delete_row(i)
        m = m.delete_column(j)
        return m.determinant()

    def delete_row(self, row):
        # clone list
        new_rows = self.rows[:]
        new_rows.pop(row)
        return Matrix(new_rows)

    def delete_column(self, column):
        new_rows = list(self.rows)
        final_rows = list()
        for i in range(len(new_rows)):
            aux_row = list(new_rows[i])
            aux_row.pop(column)
            final_rows.append(aux_row)
        return Matrix(final_rows)

    def get_cofactor_matrix(self):
        final_rows = []
        for i in range(self.m):
            aux_row = []
            for j in range(self.n):
                aux_row.append(self.cofactor(i, j))
            final_rows.append(aux_row)
        return Matrix(final_rows)

    def get_adjugate(self):
        cofactor = self.get_cofactor_matrix()
        return cofactor.get_transpose()        

    def get_inverse_matrix(self):
        """
        A**(-1) = (1/det(A)) * cofactor_transposed(A)
        """
        cofactor = self.get_cofactor_matrix()
        transposed_cofactor = cofactor.get_transpose()
        one_div_det = 1 / self.determinant()
        final = [ [i*one_div_det for i in row] for row in transposed_cofactor.rows]
        return Matrix(final)


x = Matrix([[1,2,3,4,5,6], [10,11,12,13,14,15], [-1,-2,-3,-4,-5,-6]])
y = Matrix([[1,1,1,1,1,1], [0,0,0,0,0,0], [1,2,3,4,5,6]])

print("X Matrix's rank: {0}".format(x.get_rank()))
print("X:\n{0}".format(x))

print("Y Matrix's rank: {0}".format(y.get_rank()))
print("X:\n{0}".format(x))

z = x + y
print("\nX + Y:\n{0}".format(z))

print("\nX * 2:\n{0}".format(x.scalar_product(2)))

# 3 x 5
A = Matrix([[1, 2, 3, 4, 5], [10, 11, 12, 13, 5], [-1, -2, -3, -4, 5]])
# 5 x 2
B = Matrix([[3, 4], [10, 11], [-1, -2], [4, 13], [0, 0]])

print("A {0}:\n{1}".format(A.get_rank(), A))
print("B {0}:\n{1}".format(B.get_rank(), B))


print("A * B\n{}".format(A * B))

print(A)
print("--")
print(A.delete_row(1))
print(A.delete_column(0))

print("--")
print(A)


A = Matrix([[-2, 2, -3], [-1, 1, 3], [2, 0, -1]])
print("det(A): {0}".format(A.determinant()))

A = Matrix([[-2, 2, -3], [-1, 1, 3], [2, 0, -1]])
print("det(A): {0}".format(A.determinant()))

A = Matrix([[1,   2,  3, 4],
            [8,   5,  6, 7],
            [9,  12, 10, 11],
            [13, 14, 16, 15]])
print("det(A): {0}".format(A.determinant()))

A = Matrix([[ 1,  2,  3,  4],
            [ 5,  6,  7,  8],
            [ 9, 10, 11, 12],
            [13, 14, 15, 16]])
print("det(A): {0}".format(A.determinant()))

A = Matrix([[1, 2, 3, 4, 1], [8, 5, 6, 7, 2], [9, 12, 10, 11, 3], [13, 14, 16, 15, 4], [10, 8, 6, 4, 2]])
print("det(A): {0}".format(A.determinant()))

A = Matrix([[1, 2, 3], [0, 4, 5], [1, 0, 6]])
print("---\n{0}\n---".format(A))

print("---\n{0}\n---".format(A.get_inverse_matrix()))

A = Matrix([[3, 0, 2], [2, 0, -2], [0, 1, 1]])
print("Adjugate A:\n{0}".format(A.get_adjugate()))
